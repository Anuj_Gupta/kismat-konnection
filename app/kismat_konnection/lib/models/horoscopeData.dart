class HoroscopeData {
  String imagePath;
  String titleTxt;
  String startColor;
  String endColor;
  List<String> meals;
  int kacl;

  HoroscopeData({
    this.imagePath = '',
    this.titleTxt = '',
    this.startColor = "",
    this.endColor = "",
    this.meals,
    this.kacl = 0,
  });

  static List<HoroscopeData> tabIconsList = [
    HoroscopeData(
      imagePath: 'assets/kismat_konnection/aries.png',
      titleTxt: 'Aries',
      kacl: 525,
      meals: ["Bread,", "Peanut butter,", "Apple"],
      startColor: "#FFC144",
      endColor: "#FFDA8F",
    ),
    HoroscopeData(
      imagePath: 'assets/kismat_konnection/taurus.png',
      titleTxt: 'Taurus',
      kacl: 602,
      meals: ["Salmon,", "Mixed veggies,", "Avocado"],
      startColor: "#C68A65",
      endColor: "#DDB9A3",
    ),
    HoroscopeData(
      imagePath: 'assets/kismat_konnection/gemini.png',
      titleTxt: 'Gemini',
      kacl: 0,
      meals: ["Recommend:", "800 kcal"],
      startColor: "#009688",
      endColor: "#4DB6AC",
    ),
    HoroscopeData(
      imagePath: 'assets/kismat_konnection/cancer.png',
      titleTxt: 'Cancer',
      kacl: 0,
      meals: ["Recommend:", "703 kcal"],
      startColor: "#DB71A9",
      endColor: "#E9AACB",
    ),
    HoroscopeData(
      imagePath: 'assets/kismat_konnection/leo.png',
      titleTxt: 'Leo',
      kacl: 0,
      meals: ["Recommend:", "703 kcal"],
      startColor: "#00ACC1",
      endColor: "#4DD0E1",
    ),
    HoroscopeData(
      imagePath: 'assets/kismat_konnection/virgo.png',
      titleTxt: 'Virgo',
      kacl: 0,
      meals: ["Recommend:", "703 kcal"],
      startColor: "#88DF8E",
      endColor: "#B8ECBB",
    ),
    HoroscopeData(
      imagePath: 'assets/kismat_konnection/libra.png',
      titleTxt: 'Libra',
      kacl: 0,
      meals: ["Recommend:", "703 kcal"],
      startColor: "#60A4FF",
      endColor: "#A0C8FF",
    ),
    HoroscopeData(
      imagePath: 'assets/kismat_konnection/scorpio.png',
      titleTxt: 'Scorpio',
      kacl: 0,
      meals: ["Recommend:", "703 kcal"],
      startColor: "#FF5A58",
      endColor: "#FF9C9B",
    ),
    HoroscopeData(
      imagePath: 'assets/kismat_konnection/sagittarius.png',
      titleTxt: 'Sagittarius',
      kacl: 0,
      meals: ["Recommend:", "703 kcal"],
      startColor: "#FFA726",
      endColor: "#FFCC80",
    ),
    HoroscopeData(
      imagePath: 'assets/kismat_konnection/capricorn.png',
      titleTxt: 'Capricorn',
      kacl: 0,
      meals: ["Recommend:", "703 kcal"],
      startColor: "#88DF8E",
      endColor: "#B8ECBB",
    ),
    HoroscopeData(
      imagePath: 'assets/kismat_konnection/aquarius.png',
      titleTxt: 'Aquarius',
      kacl: 0,
      meals: ["Recommend:", "703 kcal"],
      startColor: "#60A4FF",
      endColor: "#A0C8FF",
    ),
    HoroscopeData(
      imagePath: 'assets/kismat_konnection/pentagram.png',
      titleTxt: 'Pisces',
      kacl: 0,
      meals: ["Recommend:", "703 kcal"],
      startColor: "#FFDA8F",
      endColor: "#C68A65",
    ),
  ];
}
