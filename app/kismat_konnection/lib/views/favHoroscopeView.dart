import 'package:kismat_konnection/kismatKonnectionThemes.dart';
import 'package:kismat_konnection/main.dart';
import 'package:flutter/material.dart';
import 'package:kismat_konnection/views/horoscopeProgressView.dart';

class FavHoroscopeView extends StatelessWidget {
  final AnimationController animationController;
  final Animation animation;

  const FavHoroscopeView({Key key, this.animationController, this.animation})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: animationController,
      builder: (BuildContext context, Widget child) {
        return FadeTransition(
          opacity: animation,
          child: new Transform(
            transform: new Matrix4.translationValues(
                0.0, 30 * (1.0 - animation.value), 0.0),
            child: Padding(
              padding: const EdgeInsets.only(
                  left: 24, right: 24, top: 16, bottom: 18),
              child: Container(
                decoration: BoxDecoration(
                  color: KismatKonnectionThemes.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(18.0),
                      bottomLeft: Radius.circular(18.0),
                      bottomRight: Radius.circular(18.0),
                      topRight: Radius.circular(18.0)),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                        color: KismatKonnectionThemes.grey.withOpacity(0.2),
                        offset: Offset(1.1, 1.1),
                        blurRadius: 10.0),
                  ],
                ),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 6, left: 5),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(2, 0, 2, 2),
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Padding(
                                        padding: EdgeInsets.all(8.0),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Padding(
                                              padding: EdgeInsets.only(
                                                  left: 4, bottom: 2),
                                              child: Text(
                                                'Aries',
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    fontFamily:
                                                        KismatKonnectionThemes
                                                            .fontName,
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 16,
                                                    letterSpacing: -0.1,
                                                    color:
                                                        KismatKonnectionThemes
                                                            .darkerText),
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: <Widget>[
                                      Padding(
                                        padding: EdgeInsets.fromLTRB(12,2,8,5),
                                        child: 
                                        Text(
                                          "The energy is unusually slow, causing you to drag and your productivity to suffer. There isn't much you can do about it. Prioritize your tasks so you accomplish what absolutely needs to be done. Everything else will have to wait until your 'get up and go' returns tomorrow. In the meantime, energize yourself as much as possible with good nutrition and a brisk walk.",
                                          textAlign: TextAlign.justify,
                                          style: TextStyle(
                                            fontFamily:
                                                KismatKonnectionThemes.fontName,
                                            fontWeight: FontWeight.w600,
                                            fontSize: 14,
                                            color: KismatKonnectionThemes.grey
                                                .withOpacity(0.8),
                                          ),
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          left: 24, right: 24, top: 8, bottom: 8),
                      child: Container(
                        height: 2,
                        decoration: BoxDecoration(
                          color: KismatKonnectionThemes.background,
                          borderRadius: BorderRadius.all(Radius.circular(4.0)),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(2, 2, 2, 2),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              // crossAxisAlignment: CrossAxisAlignment.,
                              children: <Widget>[
                                Text(
                                  'Health',
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    fontFamily: KismatKonnectionThemes.fontName,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 16,
                                    letterSpacing: -0.2,
                                    color: KismatKonnectionThemes.darkText,
                                  ),
                                ),
                                HoroscopeProgressView(
                                  animation: animation,
                                  animationController: animationController,
                                  progressColor: "#87A0E5",
                                  progressPer: 40,
                                )
                              ],
                            ),
                          ),
                          Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'Love',
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    fontFamily: KismatKonnectionThemes.fontName,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 16,
                                    letterSpacing: -0.2,
                                    color: KismatKonnectionThemes.darkText,
                                  ),
                                ),
                                HoroscopeProgressView(
                                  animation: animation,
                                  animationController: animationController,
                                  progressColor: "#F56E98",
                                  progressPer: 80,
                                )
                              ],
                            ),
                          ),
                          Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'Luck',
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    fontFamily: KismatKonnectionThemes.fontName,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 16,
                                    letterSpacing: -0.2,
                                    color: KismatKonnectionThemes.darkText,
                                  ),
                                ),
                                HoroscopeProgressView(
                                  animation: animation,
                                  animationController: animationController,
                                  progressColor: "#F1B440",
                                  progressPer: 60,
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
