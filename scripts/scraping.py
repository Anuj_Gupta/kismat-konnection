import requests
import urllib.request
import time
from bs4 import BeautifulSoup
import pendulum
import pymongo
from pymongo import MongoClient
from datetime import datetime
client = MongoClient(
    "mongodb://horoscopeadmin:horo987654321@ds331758.mlab.com:31758/horoscope?retryWrites=false")
db = client.horoscope


url = 'https://www.ganeshaspeaks.com/horoscopes/'
frequency_type = ['daily', 'tomorrow', 'yesterday',
                  'weekly', 'monthly', 'yearly']
list_horoscope = ['aries', 'taurus', 'gemini', 'cancer', 'leo', 'virgo',
                  'libra', 'scorpio', 'sagittarius', 'capricorn', 'aquarius', 'pisces']

extract_horoscope_data = []


def scrap():
    for horoscope in list_horoscope:
        for fr in frequency_type:
            print(horoscope, "-", fr)
            response = requests.get(
                "{}{}-horoscope/{}".format(url, fr, horoscope))
            soup = BeautifulSoup(response.text, "html.parser")
            time = get_time_frame(fr)
            extract_horoscope(soup, horoscope, time)

    # print(*extract_horoscope_data, sep="\n")
    insert_horoscope()


def extract_horoscope(soup, horoscope, time):
    horoscope_text = soup.find(class_='margin-top-xs-0')
    extract_horoscope_data.append(
        {"horoscope_type": horoscope.upper(), "time_frame": time[0], "from_date": time[1], "to_date": time[2], "horoscope_text": horoscope_text.text})


def get_time_frame(frequency):
    date = datetime.now()
    current_date = pendulum.instance(date)

    time_frame = {
        "daily": ["DAILY", current_date.start_of('day'), current_date.end_of('day')],
        'tomorrow': ["TOMORROW", current_date.add(1).start_of('day'), current_date.add(1).end_of('day')],
        'yesterday': ["YESTERDAY", current_date.add(-1).start_of('day'), current_date.add(-1).end_of('day')],
        'weekly': ["WEEKLY", current_date.start_of('week'), current_date.end_of('week')],
        'monthly': ["MONTHLY", current_date.start_of('month'), current_date.end_of('month')],
        'yearly': ["YEARLY", current_date.start_of('year'), current_date.end_of('year')]
    }
    return time_frame.get(frequency)


def insert_horoscope():
    posts = db.details_horoscope
    for horoscope_data in extract_horoscope_data:
        query = {"from_date": horoscope_data.get("from_date"), "to_date": horoscope_data.get("to_date"),
                 "horoscope_type": horoscope_data.get("horoscope_type")}
        update = posts.update(query, horoscope_data, upsert=True)
        print(update)

if __name__ == "__main__":
    scrap()
